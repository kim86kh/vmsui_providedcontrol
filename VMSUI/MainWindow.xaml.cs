﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DMSkin.WPF;

using VideoOS.Platform;
using VideoOS.Platform.Client;
using VideoOS.Platform.SDK;
using VideoOS.Platform.SDK.Platform;

namespace VMSUI
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow 
    {

        List<ImageViewerWpfControl> ViewList;
        List<Item> SDKCamList;
        SDK _sdk = SDK.Instance;
        bool ImageMouseUp = false;
        int ClickedCamera = -1;
        int CurrentSingleView = -1;
        int CurrentGridRows;
        int CurrentGridCols;

        FQID playbackid = null;

        public MainWindow()
        {
            InitializeComponent();

            jmonlogo.Source = new BitmapImage(new Uri("JMON_PNG.png", UriKind.Relative));

            _sdk.LoginAsCurrentUser("http://localhost");
            SDKCamList = _sdk.GetPhysicalCameraList();
            Console.WriteLine(SDKCamList.Count + "Cameras Detected");

            ViewList = new List<ImageViewerWpfControl>();
            MainGrid.AllowDrop = true;
            MainGrid.Drop += DropOntoGrid;
            AddCameraToTreeView();
            
            //CameraListView
            


        }

        private void DropOntoGrid(object sender, DragEventArgs e)
        {
            
            MessageBox.Show("adsg");
        }

        private void AddSingleView(int idx)
        {
         
        }

        private void AddCameraToTreeView()
        {
            //CameraListView
            TreeViewItem root = new TreeViewItem();
            root.Header = "카메라";

            for (int i = 0; i < SDKCamList.Count; i++)
            {
                TreeViewItem tv = new TreeViewItem();
                tv.Header = SDKCamList[i].Name;
                
                root.Items.Add(tv);
            }
            CameraListView.Items.Add(root);
            //CameraListView.


          
        }
        private void SetCameraGridCol(int value)
        {
            int current = MainGrid.ColumnDefinitions.Count;
            if (value < 0) return;
            if (value < current)
            {
                for (int i = 0; i < current - value; i++)
                {
                    MainGrid.ColumnDefinitions.RemoveAt(MainGrid.ColumnDefinitions.Count - 1);
                }
            }
            else
            {
                for (int i = 0; i < value - current; i++)
                {
                    ColumnDefinition r = new ColumnDefinition();
                    r.Width = new GridLength(1, GridUnitType.Star);
                    MainGrid.ColumnDefinitions.Add(r);
                }
            }
            CurrentGridCols = value;
        }

        private void SetCameraGridRow(int value)
        {
            int current = MainGrid.RowDefinitions.Count;
            if (value < 0) return;
            if (value < current)
            {
                for (int i = 0; i < current - value; i++)
                {
                    MainGrid.RowDefinitions.RemoveAt(MainGrid.RowDefinitions.Count - 1);
                }
            }
            else
            {
                for (int i = 0; i < value - current; i++)
                {
                    RowDefinition r = new RowDefinition();
                    r.Height = new GridLength(1, GridUnitType.Star);
                    MainGrid.RowDefinitions.Add(r);
                }
            }
            CurrentGridRows = value;
        }

        private void SetGrid1(object sender, RoutedEventArgs e)
        {

        }

        private ImageViewerWpfControl CreateImageView(int row, int col, Item item, int tag)
        {
            ImageViewerWpfControl i = new ImageViewerWpfControl();
            
            i.Name = "ImageView_" + tag.ToString();
            i.EnableDigitalZoom = true;
            i.MaintainImageAspectRatio = true;
            i.EnableVisibleHeader = false;
            i.EnableVisibleCameraName = false;
            i.EnableVisibleLiveIndicator = false;
            i.EnableVisibleTimestamp = false;
            i.EnableSetupMode = true;

            i.Tag = tag;
            i.CameraFQID = item.FQID;
            i.PlaybackControllerFQID = playbackid;
            


            i.Initialize();
            i.Connect();
            i.StartLive();
            MainGrid.Children.Add(i);
            Grid.SetRow(i, row);
            Grid.SetColumn(i, col);
            return i;
        }


        private void MainGrid_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            /*
            if (CamList == null) return;
            double height = MainGrid.RowDefinitions[0].ActualHeight;
            double width = MainGrid.ColumnDefinitions[0].ActualWidth;
            foreach (ICamera cam in CamList)
            {
                cam.UpdateWidthHeight(width, height);
            }
          */

            Console.WriteLine("MainGrid_SizeChanged");
        }

        private void OnGridResized(object sender, SizeChangedEventArgs e)
        {

            Console.WriteLine("OnGridResized");

        }
     
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            playbackid = ClientControl.Instance.GeneratePlaybackController();
            SetCameraGridCol(4);
            SetCameraGridRow(4);
            for (int i = 0; i < CurrentGridCols*CurrentGridRows-1; i++)
            {
                ImageViewerWpfControl i1 = CreateImageView(i/CurrentGridCols, i%CurrentGridCols, SDKCamList[i%SDKCamList.Count], i);
                i1.EnableDigitalZoom = false;
                i1.RenderSize = new Size(100, 100);
                
            }

        }
    }
}
