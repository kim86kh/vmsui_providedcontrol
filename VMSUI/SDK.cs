﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using VideoOS.Platform;
using VideoOS.Platform.SDK;
using VideoOS.Platform.SDK.Platform;
namespace VMSUI
{
    class SDK
    {
        /*
         * SDK Wrapper : 
         */
        
        private static SDK _instance = null;
        public bool Loggedin { get; private set; } = false;

        public static SDK Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new SDK();
                return _instance;
            }
        }
        private SDK()
        {

        }

        public bool LoginAsCurrentUser(string server)
        {
            NetworkCredential nc = CredentialCache.DefaultNetworkCredentials;
            return LoginUsingCredential(server, nc);
        }
        public bool LoginUsingCredential(string server, NetworkCredential nc)
        {
            Uri uri = new Uri(server);
            
            try
            {
                VideoOS.Platform.SDK.Environment.Initialize();
                VideoOS.Platform.SDK.UI.Environment.Initialize();
                VideoOS.Platform.SDK.Media.Environment.Initialize();
                VideoOS.Platform.SDK.Environment.AddServer(uri, nc);
                VideoOS.Platform.SDK.Environment.Login(uri, false);
                VideoOS.Platform.SDK.Environment.LoadConfiguration(uri);
                
            }
            catch (ServerNotFoundMIPException snfe)
            {
                Console.WriteLine("Server not found");
                return false;
            }
            catch (InvalidCredentialsMIPException ice)
            {
                Console.WriteLine("invalid credential");
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                return false;
            }
            Console.WriteLine("Login success");
            Loggedin = true;
            return true;




        }

        private void FindCamera(List<Item> cave, List<Item> result)
        {
            foreach (Item i in cave)
            {
                
                if (i.FQID.Kind.Equals(Kind.Camera) && i.FQID.FolderType == FolderType.No)
                {
                    Console.WriteLine("[SDK.FindCamera] "+i.Name);
                    result.Add(i);
                }
                FindCamera(i.GetChildren(), result);
            }
        }

        public List<Item> GetPhysicalCameraList()
        {
            List<Item> result = new List<Item>();
            FindCamera(Configuration.Instance.GetItems(ItemHierarchy.SystemDefined), result);
            return result;
        }

        public void test()
        {

         
        }




    }
}
