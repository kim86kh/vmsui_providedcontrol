﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Controls;
namespace VMSUI
{
    class Container
    {
        private static Container _instance = null;

        public List<Image> ImageViewList = null;
        public List<ICamera> CameraList = null;
        public static Container Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Container();
                return _instance;
            }
        }

        private Container()
        {
            ImageViewList = new List<Image>();
            CameraList = new List<ICamera>();

        }

        
    }
}
