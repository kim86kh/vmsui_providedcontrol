﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Emgu.CV;

namespace VMSUI
{
    class Camera:ICamera
    {
        public WriteableBitmap Buffer { get; set; }
        public Mat CVBuffer { get; set; }
        public Image Parent { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public string Name { get; set; }
        public double Fps { get; set; }
        public bool _UpdateRequired { get; set; }
        public string CameraType { get; set; }

        delegate void LiveNotificationDelegate(Mat framedata);

        public void Dispose()
        {
            
        }
        public Camera(Image parent)
        {
            this.Parent = parent;
        }
        public void GrabbedFrame(Mat framedata)
        {
            if (Parent != null)
                Parent.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new LiveNotificationDelegate(DoLiveNotification), framedata);
        }

        private void DoLiveNotification(Mat framedata)
        {
            try
            {
                if (_UpdateRequired)
                {
                    Parent.Width = this.Width;
                    Parent.Height = this.Height;

                }
                Parent.Source = BitmapSourceConvert.ToBitmapSource(framedata);
            }catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void Load(string Uri)
        {
            throw new NotImplementedException();
        }

        public void Pause()
        {
            throw new NotImplementedException();
        }

        public void Play()
        {
            throw new NotImplementedException();
        }

        public void UpdateWidthHeight(double width, double height)
        {
            this.Width = width;
            this.Height = height;
            _UpdateRequired = true;
        }
    }
}
