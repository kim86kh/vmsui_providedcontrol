﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.Util;
using VideoOS.Platform;
using VideoOS.Platform.SDK;
using VideoOS.Platform.SDK.Platform;
using VideoOS.Platform.Live;
namespace VMSUI
{
    class MCamera:ICamera
    {
        public WriteableBitmap Buffer { get; set; }
        public Mat CVBuffer { get; set; }
        public Image Parent { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public string Name { get; set; }
        public double Fps { get; set; }
        public bool _UpdateRequired { get; set; }
        public string CameraType { get; set; }

        delegate void LiveNotificationDelegate(WriteableBitmap framedata);


        SDK _sdk=SDK.Instance;
        Item _item = null;
        BitmapLiveSource _live=null;

        public void Dispose()
        {
            
        }
        public MCamera(Image parent)
        {
            this.Parent = parent;
        }
        public void GrabbedFrame(WriteableBitmap framedata)
        {
            //CvInvoke.Rectangle(framedata,new System.Drawing.Rectangle(10, 10, 100, 100), new Bgr(0, 0, 255).MCvScalar,3);
            if (Parent != null)
                Parent.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new LiveNotificationDelegate(DoLiveNotification), framedata);
        }

        private void DoLiveNotification(WriteableBitmap framedata)
        {
            try
            {
                if (_UpdateRequired)
                {
                    Parent.Width = this.Width;
                    Parent.Height = this.Height;

                }
                //Parent.Source = BitmapSourceConvert.ToBitmapSource(framedata);
                Parent.Source = framedata;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void Load(Item item)
        {
            _item = item;
            this.Name = item.Name;
            
            _live = new BitmapLiveSource(item, BitmapFormat.BGR24);
            try
            {
                _live.Width = 1920;
                _live.Height = 1080;
                Console.WriteLine("Width={0} height={1}", _live.Width, _live.Height);
                Buffer = new WriteableBitmap(_live.Width, _live.Height, 0, 0, PixelFormats.Bgr24, null);
                _live.SetKeepAspectRatio(true, true);
                _live.Init();
                _live.LiveContentEvent +=OnLiveContentEvent;
                //_live.LiveStatusEvent += StatusNotification;
                _live.LiveModeStart = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to connect to recording server:" + ex.Message);
            }



        }

        private void OnLiveContentEvent(object sender, EventArgs e)
        {
            try
            {
                //카메라 영상 들어오면
                LiveContentEventArgs args = e as LiveContentEventArgs;
                if (args == null) Console.WriteLine("args null");
                LiveSourceBitmapContent bcon = args.LiveContent as LiveSourceBitmapContent;

                if (bcon == null) Console.WriteLine("bcon null");
                if (bcon != null)
                {

                    int width = bcon.GetPlaneWidth(0);
                    int height = bcon.GetPlaneHeight(0);
                    int stride = bcon.GetPlaneStride(0);

                    IntPtr plane = bcon.GetPlanePointer(0);
                    byte[] planebyte = bcon.GetPlaneBytes(0);
                    Console.WriteLine("W: {0}, H: {1}, Stride: {2}, BufSize:{3}", _live.Width, _live.Height, stride, planebyte.Length);

                    
                    WriteableBitmap _buffer = new WriteableBitmap(_live.Width, _live.Height, 0, 0, PixelFormats.Bgr24, null);
                    _buffer.WritePixels(new Int32Rect(0, 0, width, height), plane, planebyte.Length, stride);
                    //Mat _temp = new Mat(_live.Height, _live.Width, Emgu.CV.CvEnum.DepthType.Cv8U, 3);
                    //Mat output = new Mat();
                    //CvInvoke.Resize(_temp, output, Parent.RenderSize);
                    //_temp.SetTo<byte>(planebyte);
                    // planebyte = null;
                    //_buffer = null;
                    //Parent.Source = _buffer;
                    GrabbedFrame(_buffer);
                    bcon.Dispose();
                    //Console.WriteLine("image wrote");
                    

                }
            }
            catch (Exception ex)
            {
                EnvironmentManager.Instance.ExceptionDialog("BitmapLiveSourceLiveContentEvent", ex);
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
        }

        public void Pause()
        {
            throw new NotImplementedException();
        }

        public void Play()
        {
            if (_live != null)
                _live.LiveModeStart = true;
        }

        public void UpdateWidthHeight(double width, double height)
        {
            this.Width = width;
            this.Height = height;
            _UpdateRequired = true;
        }

        public void Load(string Uri)
        {
            throw new NotImplementedException();
        }
    }
}
