﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.IO;
using Emgu.CV;

namespace VMSUI
{
    class MIPCamera : ICamera
    {
        public WriteableBitmap Buffer { get; set; } = null;
        public string Name { get; set; } = null;
        public double Fps { get; set; } = 0;
        public double Width { get; set; } = 0;
        public double Height { get; set; } = 0;
        public Mat CVBuffer { get; set; } = null;
        public Image Parent { get; set; } = null;

        bool _UpdateRequired = false;
        SDK sdk = SDK.Instance;
        BitmapLiveSource _live = null;
        Item c = null;

        List<WriteableBitmap> BufferList = new List<WriteableBitmap>();
        public MIPCamera(Image parent)
        {
            this.Parent = parent;
        }
        public MIPCamera()
        {
            
        }

        public void Load(string Uri)
        {
            throw new NotImplementedException();
        }
        public void Load(Item c)
        {
            this.c = c;
            this.Name = c.Name;
            if (_live != null)
            {
                // Close any current displayed JPEG Live Source
                _live.LiveContentEvent -= LiveNotification;
                _live.LiveStatusEvent -= StatusNotification;
                _live.Close();
                _live = null;
            }
            
            _live = new BitmapLiveSource(c, BitmapFormat.BGR24);

            
            try
            {
                //z

                /*
                _live.Width = (int)_parent.Width;//이미지컨트롤 크기
                _live.Height = (int)_parent.Height;
                _live.SetWidthHeight();*/
                _live.Width = 1920;
                _live.Height = 1080;
                Console.WriteLine("Width={0} height={1}", _live.Width, _live.Height);
                Buffer = new WriteableBitmap(_live.Width, _live.Height, 0, 0, PixelFormats.Bgr24, null);
                _live.SetKeepAspectRatio(true, true);
                _live.Init();
                _live.LiveContentEvent += LiveNotification;
                _live.LiveStatusEvent += StatusNotification;
                _live.LiveModeStart = false;
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to connect to recording server:" + ex.Message);
            }

        }
        delegate void LiveNotificationDelegate( EventArgs e);
        private void DoLiveNotification( EventArgs e)
        {
            try
            {
                //카메라 영상 들어오면
                LiveContentEventArgs args = e as LiveContentEventArgs;
                if (args == null) Console.WriteLine("args null");
                LiveSourceBitmapContent bcon = args.LiveContent as LiveSourceBitmapContent;

                if (bcon == null) Console.WriteLine("bcon null");
                if (bcon != null)
                {
                    
                    int width = bcon.GetPlaneWidth(0);
                    int height = bcon.GetPlaneHeight(0);
                    int stride = bcon.GetPlaneStride(0);
                    
                    IntPtr plane = bcon.GetPlanePointer(0);
                    byte[] planebyte = bcon.GetPlaneBytes(0);
                    //Console.WriteLine("W: {0}, H: {1}, Stride: {2}, BufSize:{3}", width, height, stride, planebyte.Length);
                    Buffer.Lock();
                    Buffer.WritePixels(new Int32Rect(0, 0, width, height), plane, planebyte.Length, stride);

                    if (Parent != null)
                    {
                        if (_UpdateRequired)
                        {
                            Parent.Width = this.Width;
                            Parent.Height = this.Height;

                        }
                        Parent.Source = Buffer;
                    }
                    Buffer.Unlock();
                    bcon.Dispose();
                    //Console.WriteLine("image wrote");

                }
            }
            catch (Exception ex)
            {
                EnvironmentManager.Instance.ExceptionDialog("BitmapLiveSourceLiveContentEvent", ex);
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
        }
        private void LiveNotification(object sender, EventArgs e)
        {
            if(Parent!=null)
                Parent.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new LiveNotificationDelegate(DoLiveNotification), e );
            //Application.Current.MainWindow.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new LiveNotificationDelegate(DoLiveNotification), e);

        }
        private void StatusNotification(object sender, EventArgs e)
        {
         
        }

        public void Pause()
        {
            if (_live != null)
                _live.LiveModeStart = false;
        }

        public void Play()
        {
            if (_live != null)
                _live.LiveModeStart = true;
        }

        #region IDisposable Support
        private bool disposedValue = false; // 중복 호출을 검색하려면

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: 관리되는 상태(관리되는 개체)를 삭제합니다.
                }

                // TODO: 관리되지 않는 리소스(관리되지 않는 개체)를 해제하고 아래의 종료자를 재정의합니다.
                // TODO: 큰 필드를 null로 설정합니다.

                disposedValue = true;
            }
        }

        // TODO: 위의 Dispose(bool disposing)에 관리되지 않는 리소스를 해제하는 코드가 포함되어 있는 경우에만 종료자를 재정의합니다.
        // ~MIPCamera() {
        //   // 이 코드를 변경하지 마세요. 위의 Dispose(bool disposing)에 정리 코드를 입력하세요.
        //   Dispose(false);
        // }

        // 삭제 가능한 패턴을 올바르게 구현하기 위해 추가된 코드입니다.
        public void Dispose()
        {
            // 이 코드를 변경하지 마세요. 위의 Dispose(bool disposing)에 정리 코드를 입력하세요.
            Dispose(true);
            // TODO: 위의 종료자가 재정의된 경우 다음 코드 줄의 주석 처리를 제거합니다.
            // GC.SuppressFinalize(this);
        }

        public void UpdateWidthHeight(double width, double height)
        {
            this.Width = width;
            this.Height = height;
            _UpdateRequired = true;
        }
        #endregion
    }
}
