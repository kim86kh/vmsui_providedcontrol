﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.Util;
using VideoOS.Platform;
using VideoOS.Platform.SDK;
using VideoOS.Platform.SDK.Platform;
using VideoOS.Platform.Live;
namespace VMSUI
{
    class CCamera:ICamera
    {
        public WriteableBitmap Buffer { get; set; }
        public Mat CVBuffer { get; set; }
        public Image Parent { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public string Name { get; set; }
        public double Fps { get; set; }
        public bool _UpdateRequired { get; set; }
        public string CameraType { get; set; }

        delegate void LiveNotificationDelegate(Mat framedata);

        VideoCapture _capture = null;
        Mat _frame = null;
        public void Dispose()
        {
            
        }
        public CCamera(Image parent)
        {
            this.Parent = parent;
        }
        public void GrabbedFrame(Mat framedata)
        {
            CvInvoke.Rectangle(framedata,new System.Drawing.Rectangle(10, 10, 100, 100), new Bgr(0, 0, 255).MCvScalar,3);
            if (Parent != null)
                Parent.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new LiveNotificationDelegate(DoLiveNotification), framedata);
        }

        public void Load(string Uri)
        {
            _capture = new VideoCapture();
            _frame = new Mat();
            _capture.ImageGrabbed += _capture_ImageGrabbed;
        }

        private void _capture_ImageGrabbed(object sender, EventArgs e)
        {
           if(_capture!=null&&_capture.Ptr!=IntPtr.Zero)
            {
                _capture.Retrieve(_frame);
                GrabbedFrame(_frame);
            }
        }

        private void DoLiveNotification(Mat framedata)
        {
            try
            {
                if (_UpdateRequired)
                {
                    Parent.Width = this.Width;
                    Parent.Height = this.Height;

                }
                
                Parent.Source = BitmapSourceConvert.ToBitmapSource(framedata);
            }catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void Load(Item item)
        {


        }

        private void OnLiveContentEvent(object sender, EventArgs e)
        {
      
        }

        public void Pause()
        {
            throw new NotImplementedException();
        }

        public void Play()
        {
            if (_capture != null)
                _capture.Start();
        }

        public void UpdateWidthHeight(double width, double height)
        {
            this.Width = width;
            this.Height = height;
            _UpdateRequired = true;
        }

      
    }
}
